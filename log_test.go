package log

import (
	"bytes"
	"errors"
	"io/ioutil"
	"net/http"
	"strings"
	"testing"
)

func check(err error) {
	if err != nil {
		panic(err)
	}
}

func BenchmarkMsgWrite(b *testing.B) {
	b.RunParallel(func(pb *testing.PB) {
		for pb.Next() {
			Msg("no context").Write()
		}
	})
}

func TestFailed(t *testing.T) {
	HOSTNAME = "hostname"
	ENVIRONMENT = "production"
	body := bytes.NewBufferString(`{"body_field":"body_value"}`)
	req, err := http.NewRequest("POST", "http://localhost:8080/?query_name=query_value", body)
	check(err)
	reqBody, err := ioutil.ReadAll(req.Body)
	check(err)
	req.Header.Set("Content-Type", "application/json")

	e := Failed("test").
		RefID("123").
		User("user_name").
		Err(errors.New("new error")).
		Ctx(map[string]string{"data": "context_data"}).
		Req(req.Method, req.Host, req.URL.Path, req.URL.Query(), req.Header, reqBody).
		Resp(200, nil, nil)
	l := e.String()
	if !strings.Contains(l, "failed test") {
		t.Error("message format")
	}
	if !strings.Contains(l, "123") {
		t.Error("no reference")
	}
	if !strings.Contains(l, "user_name") {
		t.Error("no user name")
	}
	if !strings.Contains(l, "new error") {
		t.Error("no err")
	}
	if !strings.Contains(l, "context_data") {
		t.Error("no context")
	}
	if !strings.Contains(l, "hostname") {
		t.Error("no hostname")
	}
	if !strings.Contains(l, "body_field") {
		t.Error("no request body field")
	}
	if !strings.Contains(l, "body_value") {
		t.Error("no request body field value")
	}
	if !strings.Contains(l, "POST") {
		t.Error("no request method")
	}
	if !strings.Contains(l, "localhost:8080") {
		t.Error("no request host")
	}
	if !strings.Contains(l, "query_name") {
		t.Error("no request query name")
	}
	if !strings.Contains(l, "query_value") {
		t.Error("no request query value")
	}
	if !strings.Contains(l, "Content-Type") {
		t.Error("no request header name")
	}
	if !strings.Contains(l, "application/json") {
		t.Error("no request header value")
	}
	if !strings.Contains(l, "200") {
		t.Error("no response code")
	}
	if !strings.Contains(l, "production") {
		t.Error("no environment")
	}
}
